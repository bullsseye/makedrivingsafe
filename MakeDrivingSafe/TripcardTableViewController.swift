//
//  TripcardTableViewController.swift
//  MakeDrivingSafe
//
//  Created by Raman Gupta on 02/05/19.
//  Copyright © 2019 Raman Gupta. All rights reserved.
//

import UIKit
import GoogleMaps

class TripcardTableViewController: UITableViewController, TripCardCellDelegate {
    
    @IBOutlet weak var tripCardTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tripCardTableView.register(UINib(nibName: "TripCardCell", bundle: nil), forCellReuseIdentifier: "TripCardCell")
        NotificationCenter.default.addObserver(self, selector: #selector(didDriveStartQuery), name: Notification.Name.MDSDidDriveStartQueryNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didDriveEndQuery), name: Notification.Name.MDSDidDriveEndQueryNotification, object: nil)
    }
    
    // MARK: - Notification selectors
    
    @objc func didDriveStartQuery() {
        let alertController = UIAlertController (title: "Did Drive Start", message: "Did your drive start", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "YES", style: .default) { (_) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name.MDSDriveDidStartNotification, object: self)
        }
        let noAction = UIAlertAction(title: "NO", style: .default);
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil);
    }
    
    @objc func didDriveEndQuery() {
        let alertController = UIAlertController (title: "Did Drive End", message: "Did your drive end", preferredStyle: .alert);
        let yesAction = UIAlertAction(title: "YES", style: .default) { (_) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name.MDSDriveDidEndNotification, object: self);
        }
        let noAction = UIAlertAction(title: "NO", style: .default)
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TripInfo.tripInfo.keys.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> TripCardCell {
        let tripCardCell = self.tripCardTableView.dequeueReusableCell(withIdentifier: "TripCardCell", for: indexPath) as! TripCardCell
        let keys = Array(TripInfo.tripInfo.keys)
        tripCardCell.tripId = keys[indexPath.row]
        tripCardCell.delegate = self
        tripCardCell.createMap()

        return tripCardCell
    }
    
    // MARK: - TripCardCellDelegate methods
    func didRequestToCreateMapForCell(cell: TripCardCell, tripId: Int) {
        let path = GMSMutablePath()
        // FIXME: (ramang) Put some checks to validate trip ID
        //                         Sort the paths based on timestamp
        let paths = TripInfo.tripInfo[tripId]!
        
        // FIXME: (ramang) Make the constant out of this.
        let startLat = paths[0]["lat"]!
        let startLong = paths[0]["long"]!
        let camera = GMSCameraPosition.camera(withLatitude: startLat, longitude: startLong, zoom: 10.0)
        let mapView = GMSMapView.map(withFrame: cell.mapView.frame, camera: camera)
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: startLat, longitude: startLong)
        // Use reverse geocoding here.
        //        marker.title = "Sydney"
        //        marker.snippet = "Australia"
        marker.map = mapView
        
        for pathJSON in paths {
            // FIXME: (ramang) Make the constant out of this.
            path.add(CLLocationCoordinate2D(latitude: pathJSON["lat"]!, longitude: pathJSON["long"]!))
        }
        
        let line = GMSPolyline(path: path)
        line.map = mapView
        mapView.frame = cell.mapView.frame
        mapView.frame.size = CGSize(width: cell.bounds.size.width, height: cell.bounds.size.height)
        cell.mapView.addSubview(mapView)
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
