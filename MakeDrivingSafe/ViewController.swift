//
//  ViewController.swift
//  MakeDrivingSafe
//
//  Created by Raman Gupta on 02/05/19.
//  Copyright © 2019 Raman Gupta. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController {
    
    // FIXME: (ramang) Put the login functionality here later.
    let exampleString = "Raman"

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == "LoginSegue") {
            if (exampleString == "Raman") {
                return true
            }
            return false
        }
        return false
    }
}

