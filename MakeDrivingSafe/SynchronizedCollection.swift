//
//  SynchronizedCollection.swift
//  MakeDrivingSafe
//
//  Created by Raman Gupta on 02/06/19.
//  Copyright © 2019 Raman Gupta. All rights reserved.
//
import UIKit

public class SynchronizedArray<T> {
    private var array: [T] = []
    private let locationActivityLockQueue = DispatchQueue(label: "Location updates lock queue", qos: DispatchQoS.default, attributes: DispatchQueue.Attributes.concurrent)
    
    public func append(newElement: T) {
        locationActivityLockQueue.async(flags: .barrier) {
            self.array.append(newElement)
        }
    }
    
    public func removeAtIndex(index: Int) {
        locationActivityLockQueue.async(flags: .barrier) {
            self.array.remove(at: index)
        }
    }
    
    public var count: Int {
        var count = 0
        locationActivityLockQueue.sync {
            count = self.array.count
        }
        return count
    }
    
    public func first() -> T? {
        var element: T?
        locationActivityLockQueue.sync {
            if !self.array.isEmpty {
                element = self.array[0]
            }
        }
        return element
    }
    
    public subscript(index: Int) -> T {
        set {
            locationActivityLockQueue.async(flags: .barrier) {
                self.array[index] = newValue
            }
        }
        get {
            var element: T!
            locationActivityLockQueue.sync {
                element = self.array[index]
            }
            return element
        }
    }
}

public class SynchronizedQueue<T>: SynchronizedArray<T> {
    
    var size: Int
    
    override init() {
        self.size = 0
        assert(false, "Incorrect implementation called")
    }
    
    init(size: Int) {
        self.size = size
    }
    
    public override func append(newElement: T) {
        super.append(newElement: newElement)
        while (super.count > self.size) {
            super.removeAtIndex(index: 0)
        }
    }
}
