//
//  DetailMapViewController.swift
//  MakeDrivingSafe
//
//  Created by Raman Gupta on 04/05/19.
//  Copyright © 2019 Raman Gupta. All rights reserved.
//

import UIKit

class DetailMapViewController: UIViewController {
    
    private var trips : [[String: Double]] = []
        
    static func createDetialMapViewController(trips: [[String: Double]]) -> DetailMapViewController {
        let newViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailMapViewController") as!DetailMapViewController
        newViewController.trips = trips
        return newViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
