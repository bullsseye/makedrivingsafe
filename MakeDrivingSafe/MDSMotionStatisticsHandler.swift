//
//  MDSMotionStatisticsHandler.swift
//  MakeDrivingSafe
//
//  Created by Raman Gupta on 26/05/19.
//  Copyright © 2019 Raman Gupta. All rights reserved.
//

import UIKit
import CoreMotion
import CoreLocation
import CocoaLumberjack
import GLKit
import AVFoundation

// Put this into a separate file.
// Add the logging.
// Make the startLocation thread safe.
enum MDSMotionActivityState {
    case MDSMotionActivityAutomotive
    case MDSMotionActivityCycling
    case MDSMotionActivityWalking
    case MDSMotionActivityRunning
    case MDSMotionActivityStationary
    case MDSMotionActivityUnknown
}

class MDSMotionStatisticsHandler: NSObject, CLLocationManagerDelegate {
    private static let MotionActivityQueueName = "Motion activity queue"
    private static let MotionUpdatesQueueName = "Motion updates queue"
    private static let LockQueueName = "Getter setter lock queue"
    private let ChangeDeferTimer = 300.0
    private let DrivingSpeed = 0.0
    private static let SampleFrequency = 1
    private static let SampleFrequencyCalibrate = 5
    
    public static let sharedMotionStatisticsHandlerInstance = MDSMotionStatisticsHandler()
    
    // Called on the main thread
    private let motionActivityManager = CMMotionActivityManager()
    // Called on the main thread
    private let coreMotionManager = CMMotionManager()
    
    private let locationManager = CLLocationManager()
    
    // FIXME: (ramang) Use dependency injection for instantiating Algorithm.
    private var algorithm = Algorithm()
    
    private var _currentLocation: CLLocation?
    var currentLocation: CLLocation? {
        get {
            var currentLocation: CLLocation?
            lockQueue.sync {
                currentLocation = _currentLocation
            }
            return currentLocation
        }
        set {
            lockQueue.async(flags: .barrier) {
                self._currentLocation = newValue
            }
        }
    }
    
//    private var Queue: SynchronizedQueue<CLLocation>
    
    private let lockQueue = DispatchQueue(label: MDSMotionStatisticsHandler.LockQueueName, qos: DispatchQoS.default, attributes: DispatchQueue.Attributes.concurrent)
    
//    private var locationUpdates = SynchronizedArray<CLLocation>()

    private var _tempActivityState = MDSMotionActivityState.MDSMotionActivityUnknown
    var tempActivityState: MDSMotionActivityState {
        get {
            var tempActivityState = MDSMotionActivityState.MDSMotionActivityUnknown
            lockQueue.sync {
                tempActivityState = _tempActivityState
            }
            return tempActivityState
        }
        set {
            lockQueue.async(flags: .barrier) {
                self._tempActivityState = newValue
            }
        }
    }
    
    private var _motionActivityState = MDSMotionActivityState.MDSMotionActivityUnknown
    var motionActivityState: MDSMotionActivityState {
        get {
            var motionActivityState = MDSMotionActivityState.MDSMotionActivityUnknown
            lockQueue.sync {
                motionActivityState = _motionActivityState
            }
            return motionActivityState
        }
        set {
            lockQueue.async(flags: .barrier) {
                self._motionActivityState = newValue
            }
        }
    }
    
    private var _isLoggingStarted = false
    var isLoggingStarted: Bool {
        get {
            var isLoggingStarted = false
            lockQueue.sync {
                isLoggingStarted = _isLoggingStarted
            }
            return isLoggingStarted
        }
        set {
            lockQueue.async(flags: .barrier) {
                self._isLoggingStarted = newValue
            }
        }
    }
    
    // Can we increase the maxConcurrentOperationCount for motion activity updates. What will be the implications in that case.
    private var motionActivityQueue:OperationQueue = {
        var queue = OperationQueue()
        queue.name = MDSMotionStatisticsHandler.MotionActivityQueueName
        queue.maxConcurrentOperationCount = 1
        queue.qualityOfService = .default
        return queue;
    }();
    
    // Can we increase the maxConcurrentOperationCount for motion updates. What will be the implications in that case.
    private var motionUpdatesQueue:OperationQueue = {
        var queue = OperationQueue()
        queue.name = MDSMotionStatisticsHandler.MotionUpdatesQueueName
        queue.maxConcurrentOperationCount = 1
        queue.qualityOfService = .default
        return queue;
    }();
    
    private var thetaX:Double?;
    private var thetaY:Double?;
    private var thetaZ:Double?;
    private var orientationOffsetVector3OnHPlane:GLKVector3?;
    private var yawWindow = [Double]()
    private var pitchWindow = [Double]()
    private var rollWindow = [Double]()
    
    private var vxWindow = [Double]()
    private var vyWindow = [Double]()
    private var vzWindow = [Double]()
    private var speedWindow = [Double]()
    
    private override init() {
//        self.Queue = SynchronizedQueue<CLLocation>.init(size: 3)
        super.init();
        NotificationCenter.default.addObserver(self, selector: #selector(startLoggingDeviceMotionData), name: Notification.Name.MDSDriveDidStartNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(stopLoggingDeviceMotionData), name: Notification.Name.MDSDriveDidEndNotification, object: nil)
    }
    
    // Right now I am considering cycling too a automotive event. This can be changed in future
    public static func isInsideMovingVehicle(activity: MDSMotionActivityState)->Bool {
        if (activity == MDSMotionActivityState.MDSMotionActivityCycling
            || activity == MDSMotionActivityState.MDSMotionActivityAutomotive
            || activity == MDSMotionActivityState.MDSMotionActivityWalking) {
            return true
        }
        return false
    }
    
    // FIXME: (ramang) Move this method to the same file as it enum.
    public static func convertToMDSMotionActivityState(activity: CMMotionActivity)->MDSMotionActivityState {
        if (activity.automotive || activity.cycling) {
            return MDSMotionActivityState.MDSMotionActivityAutomotive
        } else if(activity.walking) {
            return MDSMotionActivityState.MDSMotionActivityWalking
        } else if(activity.running) {
            return MDSMotionActivityState.MDSMotionActivityRunning
        } else if (activity.stationary) {
            return MDSMotionActivityState.MDSMotionActivityStationary
        } else {
            return MDSMotionActivityState.MDSMotionActivityUnknown
        }
    }
    
    public func startDetectingMotionActivity() {
        weak var weakSelf = self;
        DDLogInfo("Inside startDetectingMotionActivity: isLoggingStarted: \(self.isLoggingStarted)")
        motionActivityManager.startActivityUpdates(to: self.motionActivityQueue) { (activity) in
            guard let activity = activity else {
                DDLogInfo("startDetectingMotionActivity: activity found nil")
                return
            }
            
            let tempActivityState = MDSMotionStatisticsHandler.convertToMDSMotionActivityState(activity: activity)
            DDLogInfo("Inside startDetectingMotionActivity: tempActivityState: \(tempActivityState)")
            guard let motionActivityState = weakSelf?.motionActivityState else {
                DDLogInfo("startDetectingMotionActivity: motionActivityState found nil")
                return;
            }
            
            // temp activity setter
            weakSelf?.tempActivityState = tempActivityState;
            
            let isInsideMovingVehicleTentative = MDSMotionStatisticsHandler.isInsideMovingVehicle(activity: tempActivityState);
            if (isInsideMovingVehicleTentative) {
                if (MDSMotionStatisticsHandler.isInsideMovingVehicle(activity: motionActivityState) == false) {
                    DDLogInfo("startDetectingMotionActivity: About to generate drive start query notification")
                    MDSAppHandler.generateDriveStartQueryNotification()
                }
            } else {
                if (MDSMotionStatisticsHandler.isInsideMovingVehicle(activity: motionActivityState) == true) {
                    guard let deferTimer = weakSelf?.ChangeDeferTimer else {
                        DDLogInfo("startDetectingMotionActivity: Change defer time found nil")
                        return;
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + deferTimer, execute: {
                        guard let tempActivityState = weakSelf?.tempActivityState else {
                            DDLogInfo("startDetectingMotionActivity: tempActivityState found nil")
                            return;
                        }
                        if (MDSMotionStatisticsHandler.isInsideMovingVehicle(activity: tempActivityState) == false
                            && weakSelf?.isLoggingStarted == true) {
                            DDLogInfo("startDetectingMotionActivity: About to generate drive end query notification")
                            MDSAppHandler.generateDriveEndQueryNotification()
                        }
                    })
                }
            }
        }
    }
    
    // This method should only be called from self.motionUpdatesQueue.
    private func createFeatureVector(deviceMotion:CMDeviceMotion) {
        let ax = deviceMotion.userAcceleration.x
        let ay = deviceMotion.userAcceleration.y
        let az = deviceMotion.userAcceleration.z
//        let AI = sqrt(Ax*Ax + Ay*Ay + Az*Az)
        
//        let Axh = Ax * cos(self.thetaZ!)
//        let Ayh = Ay * cos(self.thetaY!)
//        let Azh = sqrt(AI*AI - Axh*Axh - Ayh*Ayh)
        
//        let alpha = acos(Axh/AI)
//        let beta = acos(Ayh/AI)
//        let gamma = acos(Azh/AI)
        
//        let Alat = Axh*sin(alpha) + Ayh*sin(beta) + Azh*sin(gamma)
//        let Along = Axh*cos(alpha) + Ayh*cos(beta) + Azh*cos(gamma)
        
        guard let currentLocation = self.currentLocation else {
            DDLogInfo("Current Location found nil in device motion updates")
            assert(false, "Current Location found nil")
            return
        }
        
        let quaternion = deviceMotion.attitude.quaternion
        
        let glkQuaternion = GLKQuaternionMake(Float(quaternion.x),
                                              Float(quaternion.y),
                                              Float(quaternion.z),
                                              Float(quaternion.w))
        let quaternionInvert = GLKQuaternionInvert(glkQuaternion)
        
        let orientationVector3OnHPlane = GLKQuaternionRotateVector3(quaternionInvert, GLKVector3Make(10, 10, 10))
        
        // Orientation vector difference plot on horizontal plane reference.
        let diffVector3OnHPlane = GLKVector3Subtract(orientationVector3OnHPlane,
                                                     self.orientationOffsetVector3OnHPlane!)
        
//        DDLogInfo("Alat:\(Alat),Alon:\(Along),vx:\(diffVector3OnHPlane.x),vy:\(diffVector3OnHPlane.y),vz:\(diffVector3OnHPlane.z),Ax:\(Ax),Ay:\(Ay),Az:\(Az),Axh:\(Axh),Ayh:\(Ayh),Azh:\(Azh),alpha:\(alpha),beta:\(beta),gamma:\(gamma),thetaX:\(self.thetaX!),thetaY:\(self.thetaY!),thetaZ:\(self.thetaZ!)")
        let speed = currentLocation.speed*3.6
        DDLogInfo("vx:\(diffVector3OnHPlane.x),vy:\(diffVector3OnHPlane.y),vz:\(diffVector3OnHPlane.z),s:\(speed),ax:\(ax),ay:\(ay),az:\(az)")
        
        if vxWindow.count < 10 {
            vxWindow.append(Double(diffVector3OnHPlane.x))
            vyWindow.append(Double(diffVector3OnHPlane.y))
            vzWindow.append(Double(diffVector3OnHPlane.z))
            speedWindow.append(speed)
        } else {
            //FIXME:(ramang) As read swift passes by value. Keep a check if it is passed by reference.
            var vxCopy = [Double]()
            var vyCopy = [Double]()
            var vzCopy = [Double]()
            var speedCopy = [Double]()
            for vx in self.vxWindow {
                vxCopy.append(vx)
            }
            for vy in self.vyWindow {
                vyCopy.append(vy)
            }
            for vz in self.vzWindow {
                vzCopy.append(vz)
            }
            for s in self.speedWindow {
                speedCopy.append(s)
            }
            
            self.vxWindow = [Double]()
            self.vyWindow = [Double]()
            self.vzWindow = [Double]()
            self.speedWindow = [Double]()
            
            self.algorithm.classifySample(vx: vxCopy, vy: vyCopy, vz: vzCopy, speed: speedCopy)
        }
    }
    
    @objc private func startLoggingDeviceMotionData() {
        self.motionActivityState = MDSMotionActivityState.MDSMotionActivityAutomotive
        DDLogInfo("Inside startLoggingDeviceMotionData")
        self.coreMotionManager.deviceMotionUpdateInterval = 1.0 / Double(MDSMotionStatisticsHandler.SampleFrequency)
        
        DDLogInfo("startLoggingDeviceMotionData: Set deviceMotionUpdateInterval")
        self.coreMotionManager.startDeviceMotionUpdates(using: CMAttitudeReferenceFrame.xTrueNorthZVertical, to: self.motionUpdatesQueue) { (deviceMotion, error) in
            if (error != nil) {
                DDLogInfo("StartDeviceMotionUpdate: Error: \(String(describing: error))")
            } else {
                self.isLoggingStarted = true
                guard let deviceMotion = deviceMotion else {
                    DDLogInfo("Device motion found nil in device motion updates")
                    return;
                }
                
//                var location = weakSelf?.locationUpdates.first()
//                if (location == nil) {
//                    DDLogInfo("startLoggingDeviceMotionData: location found nil")
//                    location = currentLocation
//                } else {
//                    weakSelf?.locationUpdates.removeAtIndex(index: 0)
//                    currentLocation = location!
//                    DDLogInfo("startLoggingDeviceMotionData: location found !nil")
//                }
                
                self.createFeatureVector(deviceMotion:deviceMotion)
//                let drive = NetworkHandler.Drive()
//
//                drive.setAX(ax: deviceMotion.userAcceleration.x)
//                    .setAY(ay: deviceMotion.userAcceleration.y)
//                    .setAZ(az: deviceMotion.userAcceleration.z)
//                    .setRoll(roll: deviceMotion.attitude.roll)
//                    .setPitch(pitch: deviceMotion.attitude.pitch)
//                    .setYaw(yaw: deviceMotion.attitude.yaw)
//                    .setMagneticField(magneticField: deviceMotion.magneticField)
//                    .setLat(lat: location!.coordinate.latitude)
//                    .setLong(long: location!.coordinate.longitude)
//                    .setAltitude(alt: location!.altitude)
//                    .setHorizontalAccuracy(xAcc: location!.horizontalAccuracy)
//                    .setVerticalAccuracy(yAcc: location!.verticalAccuracy)
//                    .setSpeed(v: currentLocation.speed)
//                    .setTimestamp(ts: location!.timestamp)
//                    .sendDrive()
            }
        }
    }
    
    /*
     This method should only be called on motionUpdatesQueue.
     */
//    private func checkZigZag() {
//        if (self.Queue.count < 3) {
//            return
//        }
//
//        let third = self.Queue[0]
//        let second = self.Queue[1]
//        let first = self.Queue[2]
//
//        // calculate angle between first two points.
//        let degreeOne = atan2(second.coordinate.longitude - third.coordinate.longitude,
//                              second.coordinate.latitude - third.coordinate.latitude) * 180 / .pi
//
//        // calculate angle between latest two points
//        let degreeTwo = atan2(first.coordinate.longitude - second.coordinate.longitude,
//                              first.coordinate.latitude - second.coordinate.latitude) * 180 / .pi
//
//        let changeInAngle = degreeOne - degreeTwo
//
//        let timeDifference = first.timestamp.timeIntervalSince1970 - second.timestamp.timeIntervalSince1970
//
//        // Check what needs to be logged here.
//        let speed = first.speed
//        let prevSpeed = second.speed
//        let hAcc = first.horizontalAccuracy
//        let vAcc = first.verticalAccuracy
//        let phAcc = second.horizontalAccuracy
//        let pvAcc = second.verticalAccuracy
//
//        if timeDifference <= 2 {
//            DDLogInfo("Output=angleD:\(changeInAngle),yawD:\(changeInYaw),rollD:\(changeInRoll),pitchD:\(changeInPitch),v:\(speed),pV:\(prevSpeed),hAcc:\(hAcc),vAcc:\(vAcc),phAcc:\(phAcc),pvAcc:\(pvAcc),tsDiff:\(timeDifference)")
//        } else {
//            DDLogInfo("Discard changeInAngle due time > 2")
//        }
//    }
    
    @objc private func stopLoggingDeviceMotionData() {
        DDLogInfo("Inside stopLoggingDeviceMotionData")
        // As of now, I am only concerned whether the state is automative or not.
        self.motionActivityState = MDSMotionActivityState.MDSMotionActivityUnknown
        self.isLoggingStarted = false
        self.coreMotionManager.stopDeviceMotionUpdates()
    }
    
    // MARK - Location manager methods
    
    public func authorizeLocationManager() {
        DDLogInfo("Inside authorizeLocationManager")
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if (authorizationStatus == CLAuthorizationStatus.notDetermined) {
            DDLogInfo("Location access authorization status = notDetermined")
            locationManager.requestAlwaysAuthorization()
        } else if (authorizationStatus == CLAuthorizationStatus.authorizedAlways) {
            DDLogInfo("Location access authorization status = authorizedAlways")
            self.setupLocationManager()
        }
    }
    
    private func setupLocationManager() {
        DDLogInfo("Inside setupLocationManager")
        weak var weakSelf = self
        DispatchQueue.global(qos: .default).async {
            // For our purpose we need approximate coordiantes and approximate speed of the device.
            weakSelf?.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            weakSelf?.locationManager.distanceFilter = kCLDistanceFilterNone
            weakSelf?.locationManager.delegate = self
            weakSelf?.locationManager.allowsBackgroundLocationUpdates = true
            weakSelf?.currentLocation = nil
            
            weakSelf?.locationManager.startUpdatingLocation()
        }
    }
    
    // MARK - Location manager delegate methods.
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        DDLogInfo("Inside didChangeAuthorization delegate method: status: \(status)")
        if (status == CLAuthorizationStatus.authorizedAlways) {
            self.setupLocationManager()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // The updates will come on the default queue.
        DDLogInfo("Inside didUpdateLocations delegate method: isLoggingStarted: \(self.isLoggingStarted), currentLocation: \(String(describing: self.currentLocation))")
        
        if (!self.isLoggingStarted) {
            let speedOfDrivingLocations = locations.filter { $0.speed > DrivingSpeed}
            if (speedOfDrivingLocations.count > 0) {
                DDLogInfo("locationManager: didUpdatelocation: driving speed detected")
                self.startDetectingMotionActivity()
            }
        }
        self.currentLocation = locations[0]
//        else {
//            self.currentLocation = locations[0]
//            for location in locations {
//                self.Queue.append(newElement: location)
//            }
//        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DDLogInfo("locationManager: didFailWithError: error: \(error)")
    }
    
    // MARK - Calibrate methods
    // FIXME: (ramang) Move this to MotionActivityManager
    // FIXME: (ramang) Remove the code due to horizontal plane calibration.
    func calibrate() {
        var offsetQuaternion:CMQuaternion?;
        
        self.coreMotionManager.deviceMotionUpdateInterval = 1.0 / Double(MDSMotionStatisticsHandler.SampleFrequencyCalibrate)
        
        self.coreMotionManager.startDeviceMotionUpdates(using: CMAttitudeReferenceFrame.xTrueNorthZVertical, to: self.motionUpdatesQueue) { (deviceMotion, error) in
            print("Inside calibrate: deviceMotionUpdates")
            if (error != nil) {
                DDLogInfo("calibrate: error: \(String(describing: error))")
            } else {
                if self.yawWindow.count < MDSMotionStatisticsHandler.SampleFrequencyCalibrate {
                    if deviceMotion != nil {
                        self.yawWindow.append(deviceMotion!.attitude.yaw)
                        self.pitchWindow.append(deviceMotion!.attitude.pitch)
                        self.rollWindow.append(deviceMotion!.attitude.roll)
                    }
                } else {
                    if self.thetaZ == nil {
                        self.thetaZ = self.rollWindow.reduce(0, +) / Double(MDSMotionStatisticsHandler.SampleFrequencyCalibrate)
                        self.thetaY = self.pitchWindow.reduce(0, +) / Double(MDSMotionStatisticsHandler.SampleFrequencyCalibrate)
                        self.thetaX = self.yawWindow.reduce(0, +) / Double(MDSMotionStatisticsHandler.SampleFrequencyCalibrate)
                        
                        if deviceMotion != nil {
                            offsetQuaternion = deviceMotion!.attitude.quaternion
                            self.coreMotionManager.stopDeviceMotionUpdates()
                            
                            assert(offsetQuaternion != nil, "Initial quaternion found nil")
                            let glkQuaternion = GLKQuaternionMake(Float(offsetQuaternion!.x),
                                                                  Float(offsetQuaternion!.y),
                                                                  Float(offsetQuaternion!.z),
                                                                  Float(offsetQuaternion!.w))
                            let quaternionInvert = GLKQuaternionInvert(glkQuaternion)
                            self.orientationOffsetVector3OnHPlane = GLKQuaternionRotateVector3(quaternionInvert, GLKVector3Make(10, 10, 10))
                            assert(self.thetaX != nil, "thetaX found nil: Calibration did not happen")
                            assert(self.thetaY != nil, "thetaY found nil: Calibration did not happen")
                            assert(self.thetaZ != nil, "thetaZ found nil: Calibration did not happen")
                            assert(self.orientationOffsetVector3OnHPlane != nil, "orientationOffsetVector3OnHPlane found nil: Calibration did not happen")
                            DDLogInfo("Calibrate asserts did not fail")
                            self.authorizeLocationManager()
                        }
                    }
                }
            }
        }
    }
}
