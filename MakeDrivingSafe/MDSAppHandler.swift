//
//  MDSAppHandler.swift
//  MakeDrivingSafe
//
//  Created by Raman Gupta on 26/05/19.
//  Copyright © 2019 Raman Gupta. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CocoaLumberjack

extension NSNotification.Name {
    public static let MDSDidDriveStartQueryNotification = Notification.Name("MDSDidDriveStartQueryNotification")
    public static let MDSDidDriveEndQueryNotification = Notification.Name("MDSDidDriveEndQueryNotification")
    public static let MDSDriveDidStartNotification = Notification.Name("MDSDriveDidStartNotification")
    public static let MDSDriveDidEndNotification = Notification.Name("MDSDriveDidEndNotification")
}

/**
 This class contains the App level properties and routing logic. Deeplinks are used to route to the appropriate viewcontroller.
 All the initializations are done after didFinishLaunching in the app delegate.
 */
class MDSAppHandler: NSObject {
    
    private static let LockQueueName = "MDSAppHandlerLockQueue"
    static let sharedInstance = MDSAppHandler();
    private static let GMSServiceAPIKey = "AIzaSyCa-mbh5MclGs5qriBcNbk0xUq6C-EajAk";
    private static let GMSPlacesClientAPIKey = "AIzaSyCa-mbh5MclGs5qriBcNbk0xUq6C-EajAk";
    private let lockQueue = DispatchQueue(label: MDSAppHandler.LockQueueName, qos: DispatchQoS.default, attributes: DispatchQueue.Attributes.concurrent)
    private static let CalibrateBufferTime = 5.0
    
    private override init() {
    }
    
    public static func generateDriveStartQueryNotification() {
        DDLogInfo("MDSAppHandler: Inside generateDriveStartQueryNotification")
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name.MDSDidDriveStartQueryNotification, object: self)
        }
    }
    
    public static func generateDriveEndQueryNotification() {
        DDLogInfo("MDSAppHandler: Inside generateDriveEndQueryNotification")
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name.MDSDidDriveEndQueryNotification, object: self)
        }
    }
    
    public func initializeAtAppLaunch() {
        DDLogInfo("MDSAppHandler: Inside initializeAtAppLaunch")
        // Register the API keys.
        GMSServices.provideAPIKey(MDSAppHandler.GMSServiceAPIKey);
        GMSPlacesClient.provideAPIKey(MDSAppHandler.GMSPlacesClientAPIKey);
        
        // I will do the calibration here.
        // with the wait block of 10 seconds, I will call the calibration method.
        // FIXME: (ramang) Make the constant out of 10 seconds.
        DispatchQueue.main.asyncAfter(deadline: .now() + MDSAppHandler.CalibrateBufferTime) {
            MDSMotionStatisticsHandler.sharedMotionStatisticsHandlerInstance.calibrate()
        }
    }
}
