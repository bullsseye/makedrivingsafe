//
//  AppDelegate.swift
//  MakeDrivingSafe
//
//  Created by Raman Gupta on 02/05/19.
//  Copyright © 2019 Raman Gupta. All rights reserved.
//

import UIKit
import CoreData
import Fabric
import Crashlytics
import CocoaLumberjack
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let appHandler = MDSAppHandler.sharedInstance
    
    // Put the file logger in logging manager.
    let fileLogger: DDFileLogger = DDFileLogger.init(logFileManager: DDLogFileManagerDefault.init(logsDirectory: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]))
    
    private func setupLogger() {
        DDLog.add(DDTTYLogger.sharedInstance, with: DDLogLevel.info)
        // File logger
        fileLogger.rollingFrequency = TimeInterval(60*60*24)
        fileLogger.logFileManager.maximumNumberOfLogFiles = 7
        DDLog.add(fileLogger, with: .info)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.setupLogger()
        
        // Singleton NetworkHandler seems to initializing lazily even though it is created in a way for
        // early initialization. Since its presence is required as soon as the app starts, calling the
        // shared instance below to create its instance.
        let _ = NetworkHandler.sharedInstance
        let locationUpdate = launchOptions?[UIApplication.LaunchOptionsKey.location]
        DDLogInfo("application: didFinishLaunchingWithOptions: locationUpdate: \(String(describing: locationUpdate))")
        appHandler.initializeAtAppLaunch()
        Fabric.with([Crashlytics.self])
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback,
                                                        mode: AVAudioSession.Mode.default,
                                                        options: AVAudioSession.CategoryOptions.duckOthers)
        } catch {
            DDLogInfo("Failed to set AVAudioSessionCategory")
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        DDLogInfo("Application will resign active")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        DDLogInfo("Application did enter background")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        DDLogInfo("Application will enter foreground")
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        DDLogInfo("Application did become active")
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        DDLogInfo("Application will terminate")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "MakeDrivingSafe")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

