//
//  TripCardCell.swift
//  MakeDrivingSafe
//
//  Created by Raman Gupta on 02/05/19.
//  Copyright © 2019 Raman Gupta. All rights reserved.
//

import UIKit
import GoogleMaps

protocol TripCardCellDelegate {
    // FIXME: (ramang) check swift method naming convention
    func didRequestToCreateMapForCell(cell: TripCardCell, tripId: Int)
}

class TripCardCell: UITableViewCell, GMSMapViewDelegate {
    
    var tripId : Int?
    var delegate : TripCardCellDelegate?

    @IBOutlet weak var mapView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.frame.size = CGSize(width: self.bounds.size.width, height: self.bounds.size.height)
    }
    
    func createMap() {
        if (delegate != nil && tripId != nil) {
            self.delegate?.didRequestToCreateMapForCell(cell: self, tripId: self.tripId!)
        }
    }
}
