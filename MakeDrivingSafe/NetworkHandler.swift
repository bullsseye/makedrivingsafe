//
//  NetworkHandler.swift
//  MakeDrivingSafe
//
//  Created by Raman Gupta on 28/05/19.
//  Copyright © 2019 Raman Gupta. All rights reserved.
//

import UIKit
import CoreLocation
import CoreMotion
import Alamofire
import CocoaLumberjack

class NetworkHandler: NSObject {
    
    static let sharedInstance = NetworkHandler.init()
    private static let URLString = "https://bjcucftq49.execute-api.ap-south-1.amazonaws.com/staging"
    
    private override init() {
        super.init()
        NSLog("NetworkHandler: Registering start drive and end drive notifications")
        NotificationCenter.default.addObserver(self, selector: #selector(createDriveId), name: Notification.Name.MDSDriveDidStartNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(invalidateDriveId), name: Notification.Name.MDSDriveDidEndNotification, object: nil)
    }
    
    class Drive {
        fileprivate static var driveId: String?
        // FIXME: (ramang) Make it private again after final design.
        public var serializedDict = [String : Any]()
        
        private var roll: Double? {
            didSet {
                self.serializedDict["roll"] = self.roll
            }
        }
        private var pitch: Double? {
            didSet {
                self.serializedDict["pitch"] = self.pitch
            }
        }
        private var yaw: Double? {
            didSet {
                self.serializedDict["yaw"] = self.yaw
            }
        }
        
        private var ax: Double? {
            didSet {
                self.serializedDict["ax"] = self.ax
            }
        }
        private var ay: Double? {
            didSet {
                self.serializedDict["ay"] = self.ay
            }
        }
        private var az: Double? {
            didSet {
                self.serializedDict["az"] = self.ay
            }
        }
        
        private var magneticField: CMCalibratedMagneticField? {
            didSet {
//                self.serializedDict["magneticField"] = self.magneticField
            }
        }
        
        private var lat: CLLocationDegrees? {
            didSet {
                self.serializedDict["lat"] = self.lat
            }
        }
        private var long: CLLocationDegrees? {
            didSet {
                self.serializedDict["long"] = self.long
            }
        }
        
        private var alt: CLLocationDistance? {
            didSet {
                self.serializedDict["alt"] = self.alt
            }
        }
        
        private var xAcc: CLLocationAccuracy? {
            didSet {
                self.serializedDict["xAcc"] = self.xAcc
            }
        }
        private var yAcc: CLLocationAccuracy? {
            didSet {
                self.serializedDict["yAcc"] = self.yAcc
            }
        }
        private var ts: TimeInterval? {
            didSet {
                self.serializedDict["ts"] = self.ts
            }
        }
        private var v: CLLocationSpeed? {
            didSet {
                self.serializedDict["v"] = self.v
            }
        }
        
        public func setRoll(roll: Double) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.roll = roll
            return self
        }
        
        public func setPitch(pitch: Double) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.pitch = pitch
            return self
        }
        
        public func setYaw(yaw: Double) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.yaw = yaw
            return self
        }
        
        public func setAX(ax: Double) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.ax = ax
            return self
        }
        
        public func setAY(ay: Double) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.ay = ay
            return self
        }
        
        public func setAZ(az: Double) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.az = az
            return self
        }
        
        public func setMagneticField(magneticField: CMCalibratedMagneticField) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.magneticField = magneticField
            return self
        }
        
        public func setLat(lat: CLLocationDegrees) -> Drive {
            // How to make the asserts crash the app in production?
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.lat = lat
            return self
        }
        
        public func setLong(long: CLLocationDegrees) -> Drive {
            // How to make the asserts crash the app in production?
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.long = long
            return self
        }
        
        public func setAltitude(alt: CLLocationDistance) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.alt = alt
            return self
        }
        
        public func setHorizontalAccuracy(xAcc: CLLocationAccuracy) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.xAcc = xAcc
            return self
        }
        
        public func setVerticalAccuracy(yAcc: CLLocationAccuracy) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.yAcc = yAcc
            return self
        }
        
        public func setSpeed(v: CLLocationSpeed) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.v = v
            return self
        }
        
        public func setTimestamp(ts: Date) -> Drive {
            assert(Drive.driveId != nil, "Drive ID not yet created")
            self.ts = ts.timeIntervalSince1970
            return self
        }
        
        // FIXME: (ramang) Uncomment it later when the data needs to be sent to the server.
        public func sendDrive() {
//            let url = URL(string: NetworkHandler.URLString)!
//            self.serializedDict["DriveId"] = Drive.driveId
//
//            Alamofire.request(url, method: .post, parameters: self.serializedDict, encoding: JSONEncoding.default, headers: [:]).responseJSON { (response) in
//                if (!response.result.isSuccess) {
//                    DDLogInfo("Response failed with error: \(String(describing: response.result.error)) for drive: \(self.serializedDict)")
//                }
//            }
        }
    }
    
    @objc private func invalidateDriveId() {
        Drive.driveId = nil
    }
    
    private static func generateDriveId() -> String {
        var driveId = ""
        
        // First create the lower case characters
        for _ in 0..<4 {
            let randomInt = Int.random(in: 0..<26)
            driveId += String((UnicodeScalar(Int(UnicodeScalar("a").value) + randomInt)!))
        }
        
        // Add a number
        driveId += String(Int.random(in: 0..<10000000000))
        
        // Add uppercase character
        let randomInt = Int.random(in: 0..<26)
        driveId += String((UnicodeScalar(Int(UnicodeScalar("A").value) + randomInt)!))
        
        return driveId
    }
    
    @objc private func createDriveId() {
        Drive.driveId = NetworkHandler.generateDriveId()
    }
}
