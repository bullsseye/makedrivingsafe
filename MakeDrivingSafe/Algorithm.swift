//
//  Algorithm.swift
//  MakeDrivingSafe
//
//  Created by Raman Gupta on 24/09/19.
//  Copyright © 2019 Raman Gupta. All rights reserved.
//

import UIKit
import AVFoundation
import CocoaLumberjack

class Algorithm: NSObject {
    
    private static let UTurnMagThreshold = 20
    private static let TurnMagThreshold = 7
    private static let DriftMagThreshold = 3
    
    private static let UTurnSpeedThreshold = 20.0
    private static let TurnSpeedThreshold = 30.0
    private static let DriftSpeedThreshold = 40.0
    
    private static let WindowSize = 10
    private var abnormalDriftCountInMinute = 0
    private var timer = Timer()
    
    private let synthesiser = AVSpeechSynthesizer()
    
    func classifySample(vx: [Double], vy: [Double], vz: [Double], speed: [Double]) {
    
        assert(vx.count>0 && vx.count<=10, "vx not in array limits")
        assert(vy.count>0 && vy.count<=10, "vy not in array limits")
        assert(vz.count>0 && vz.count<=10, "vz not in array limits")
        
        DispatchQueue.global(qos: .userInitiated).sync {
            var vx_diff = [0.0]
            var vy_diff = [0.0]
            var vz_diff = [0.0]
            
            vx_diff.append(contentsOf: zip(vx.dropFirst(), vx).map(-))
            vy_diff.append(contentsOf: zip(vy.dropFirst(), vy).map(-))
            vz_diff.append(contentsOf: zip(vz.dropFirst(), vz).map(-))
            
            let mag_first_two = zip(vx_diff, vy_diff).map({sqrt($0*$0 + $1*$1)})
            let mag_double = zip(mag_first_two, vz_diff).map({sqrt($0*$0 + $1*$1)})
            let mag = mag_double.map({Int($0 + 0.5)})
            
            let drift_total = mag.reduce(0, +)
            let average_speed = speed.reduce(0, +)/Double(Algorithm.WindowSize)
            
            var message = ""
            
            if drift_total >= Algorithm.UTurnMagThreshold {
                if average_speed >= Algorithm.UTurnSpeedThreshold {
                    message = "Dangerous U turn drift val: \(drift_total) speed: \(Int(average_speed))"
                } else {
                    message = "Ignore u turn drift value: \(drift_total) due to low speed: \(Int(average_speed))"
                }
            } else if drift_total >= Algorithm.TurnMagThreshold {
                if average_speed >= Algorithm.TurnSpeedThreshold {
                    message = "Dangerous turn drift val: \(drift_total) speed: \(Int(average_speed))"
                } else {
                    message = "Ignore turn drift value: \(drift_total) due to low speed: \(Int(average_speed))"
                }
            } else if drift_total >= Algorithm.DriftMagThreshold {
                if average_speed >= Algorithm.DriftSpeedThreshold {
                    if self.abnormalDriftCountInMinute == 0 {
                        self.timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(timerAction), userInfo: nil, repeats: false)
                    }
                    self.abnormalDriftCountInMinute += 1
                    message = "Lane change noted drift val: \(drift_total) speed: \(Int(average_speed))"
                } else {
                    message = "Ignore lane change drift value: \(drift_total) due to low speed: \(Int(average_speed))"
                }
            } else {
                message = "Safe drift value: \(drift_total) speed: \(Int(average_speed))"
            }
            
            // Since it can take time to speak, moving it into async.
            DispatchQueue.main.async {
                self.synthesiser.speak(AVSpeechUtterance(string: message))
            }
            // Keep it in sync as I do not want logs out of order.
            DDLogInfo(message)
        }
    }
    
    @objc func timerAction() {
        if self.abnormalDriftCountInMinute >= 3 {
            self.synthesiser.speak(AVSpeechUtterance(string: "High drift frequency detected"))
            DDLogInfo("High drift frequency detected")
        }
        // Need to verify whether I have to make abnormalDriftCountInMinute thread safe.
        self.abnormalDriftCountInMinute = 0
        timer.invalidate()
    }
}
