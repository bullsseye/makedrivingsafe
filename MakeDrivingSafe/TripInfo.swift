//
//  TripInfo.swift
//  MakeDrivingSafe
//
//  Created by Raman Gupta on 03/05/19.
//  Copyright © 2019 Raman Gupta. All rights reserved.
//

import UIKit

class TripInfo: NSObject {
        
    static let tripInfo = [1: [["ts": 1234, "lat": 56, "long":  -125, "driving_score": 5],
                               ["ts": 1235, "lat": 56.1, "long": -125.1, "driving_score": 6],
                               ["ts": 1236, "lat": 56.2, "long": -125.2, "driving_score": 7]],
                           
                           2: [["ts": 1240, "lat": 28.6047, "long": 77.0505, "driving_score": 5],
                               ["ts": 1241, "lat": 28.7, "long": 77.06, "driving_score": 6],
                               ["ts": 1242, "lat": 28.8, "long": 77.08, "driving_score": 7]]
                           ]
}
