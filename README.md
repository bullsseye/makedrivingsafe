**About**

This project is created to collect driving data to understand the driving behavior using GPS, magnetometer, gyroscope, accelerometer and characterise 
into safe, moderate and unsafe. 
It is currently able to detect lane changes, UTurns and Turns and also able to figure out rash driving at a rudimentary level. It samples the driving 
data every 10 seconds and provide voice feedback to the driver about his driving skills.

**Working**

Fix the phone in your car using the holder. Once the car starts the MakeDrivingSafe application launches itself. Once the speed becomes more than 0mps 
and motion is detected as Automotive, the application starts logging sensor data and the algorithm starts processing every 10 seconds sample to provide 
instructions.

The thresholds for lane changes, turns and uturns are calculated empirically analyzing the charts created from the combination of the sensor data 
logged. 

The x-axis w.r.t the phone reference points to the true north and z-axis vertical to the phone's horizontal plane. The device orientation changes are
calculated by quaternion given by iOS. The vector(10, 10, 10) is transformed on the horizontal plane using negation of this quaternion and then plotted.
This transformed vector gives a long term noise for any drift/lane change/Uturn/Turn. Accelerometer provides a short term noise for the same.

**Ongoing work**

*The iOS accelerometer sensors are not tuned for car accelerometer reading. Working on tuning it.

*Since mobile phone can be placed in any position, finding the relative orientation between device's frame of reference and car's frame of reference
is a challenging task.

*Calculate centrifugal force exerted during turns.

**Future work**

*Map Indian road conditions using smartphone signals.

*Create a driving score

**Processed Charts from collected data**

`The below is the safe drive chart from my home to office. The larger dips and ups in the vx, vy and vz graph in the chart represent the UTurns and Turns.
 The vx, vy and vz are the device orientation plots whereas vx_diff, vy_diff and vz_diff are the device orientation changes plot in x, y and z-axis resp.`
![Safe Drive Chart](charts/safe_drive.png?raw=true "Safe Drive Chart")


`The below chart shows zig-zag driving from time 55-118, 325-370, 379-397, 487-532. The variations in the vx, vy and vz plots in these time frames depict
lane changes and zig-zag drive`
![Zig-Zag-drive Chart](charts/zig-zag drive.png?raw=true "Zig-zag Drive Chart")

